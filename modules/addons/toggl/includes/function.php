<?php
use Illuminate\Database\Capsule\Manager as Capsule;

function getapidata($url, $post, $header){
	  $ch = curl_init($url);
	  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
	  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	  $result = curl_exec($ch);
	  curl_close($ch);  // Seems like good practice
	  return $result;
 }

 function run_curl(){
	$addon = Capsule::table('tbladdonmodules')->where('module', 'toggl')->where('setting', 'Api_token')->first();
	$api = $addon->value;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://www.toggl.com/api/v8/workspaces");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_USERPWD, $api .":" . "api_token");
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close ($ch);
    return $res=json_decode($result,true);
}
 function convertToHoursMins($init) {
	$hours = floor($init / 1000/3600);
	$minutes = floor(($init / 60) % 60);
	$seconds = $init % 60;

	return "$hours.$minutes";
}
function run_curl_workstation($data){
    $addon = Capsule::table('tbladdonmodules')->where('module', 'toggl')->where('setting', 'Api_token')->first();
    $api = $addon->value;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://toggl.com/api/v8/workspaces/".$data['workspace_id']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_USERPWD, $api .":" . "api_token");
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close ($ch);
    return $res=json_decode($result,true);
}
function getTogglSetting($key){
    $result = Capsule::table('toggl_settings')->where('key',$key)->first();
    return $result->value;
}
?>
