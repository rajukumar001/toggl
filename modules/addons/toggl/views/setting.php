<?php
use Illuminate\Database\Capsule\Manager as Capsule;
$inc = '../modules/addons/toggl/includes/';
include_once($inc.'function.php');
$apidata = run_curl();

if(isset($_REQUEST['submit_btn'])){
	if($_POST['checkbox'] == "on"){
		$_POST['checkbox'] = 1;
	}else{
		$_POST['checkbox'] = 0;
	}
	foreach($_POST as $key => $value){
		if($key=='workstation'){
			$value=implode(',', $_POST['workstation']);
		}
		$data = Capsule::table('toggl_settings')->where('key',$key)->count();
		if($data == 0){
			Capsule::table('toggl_settings')->insert(
			    [
			 	    'key' => $key,
					'value' => $value,
					'status' => $_POST['checkbox'],
				]
			);
		}else{
			Capsule::table('toggl_settings')
				->where('key',$key)
				->update(
					[
						'key' => $key,
						'value' => $value,
						'status' => $_POST['checkbox'],
					]
			);
		}

	}
}
 ?>
<form method="post">
<table style="width:40%;margin-top: 31px;" class="table">
	  <tr>
	    <th style="width:14%" ></th>
		<th style="width:19%"><?php echo $LANG['label']; ?></th>
		<th style="width:10%; display: none">Enable/Disable</th>
	  </tr>
		<tr>
		    <td style="width:14%" name="name" value="Workstation ID"><?php echo $LANG['workspaceid']; ?></td>
			<td style="width:19%"><select multiple="multiple" id="my-select" name="workstation[]">
			<?php  $returndata=explode(',',getTogglSetting('workstation'));  //print_r($returndata);
				foreach($apidata as $api){ ?>
			<?php if(in_array($api['id'],$returndata)){
					$selected='selected="selected"';
				}else{
					$selected='';
				}?>
                 <option <?php echo $selected; ?> value='<?php echo $api['id']; ?>'><?php echo $api['name'];?></option>
			<?php  } ?>
                 </select>
			<td style="width:10%;display: none;"> <input type="checkbox" name="checkbox" class="custom-control-input" id="">
		</tr>
		<!--<tr>
			<td style="width:14%" name="name" value="Project ID">Project ID</td>
		    <td style="width:19%"><textarea name="project_id" id="project_id" ><?php echo getTogglSetting('project_id'); ?></textarea></td>
            <td style="width:10%"> <input type="checkbox" name="checkbox" class="custom-control-input" id="" ></td>
		</tr>-->
		<tr>
			<td style="width:14%" name="name" value="Project ID"><?php echo $LANG['hourlyrate']; ?></td>
		    <td style="width:19%"><input name="hourly_rate" id="hourly_rate" value= "<?php echo getTogglSetting('hourly_rate'); ?> " ></td>
           <td style="width:10%" ><br> (<?php echo $LANG['whenexecute']; ?>.) </td>
		</tr>
		<tr>
			<td></td><td><button class="btn default-btn" name= "submit_btn" type="submit"><?php echo $LANG['submitbutton']; ?></button></td>
		</tr>
</table>
</form>
