<?php
use Illuminate\Database\Capsule\Manager as Capsule;

require_once 'init.php';
function run_curl($data){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://toggl.com/reports/api/v2/details?workspace_id=".$data['workspace_id']."&since=".$data['since']."&until=".$data['until']."&user_agent=".$data['user_agent']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_USERPWD, $data['api_token'].":" . "api_token");
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close ($ch);
    return $res=json_decode($result,true);
}
function run_curl_workstation($data){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://toggl.com/api/v8/workspaces/".$data['workspace_id']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_USERPWD, $data['api_token'].":" . "api_token");
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close ($ch);
    return $res=json_decode($result,true);
}
$addon = Capsule::table('tbladdonmodules')->where('module', 'toggl')->where('setting', 'Api_token')->first();
$api = $addon->value;
$respwork = Capsule::table('toggl_settings')->where('key','workstation')->first();
$workstations=explode(',',$respwork->value);
$defaulthourly=getTogglSetting('hourly_rate');
foreach($workstations as $respdata){
    $postData = array(
        "api_token" => $api,
        "workspace_id" => $respdata,
        "since" => date('Y-m-d',strtotime('-1 day')),
        "until" => date('Y-m-d'),
        "user_agent" => "api_test",
    );
    $ress = run_curl($postData);
    foreach($ress['data'] as $toggdata){
           // print_r($toggdata);
        //if($toggdata['billable']>0){
            $workstationid=run_curl_workstation($postData);
            $hourly=$hourlyrate['data']['default_hourly_rate']==0?$defaulthourly:$workstationid['data']['default_hourly_rate'];
            $uid = $toggdata['uid'];
            $username = $toggdata['client'];
            $name = explode(' ', $username);
            $companyname = explode('-',trim($username));
            $cname='';
            if(count($companyname)==2){
                $companynametwice = explode(' ',trim($companyname[1]));
               // print_r($companynametwice);
                if(count($companynametwice)>=2){
                    $cname=$companynametwice[0];
                }else{
                    $cname=$companyname[1];
                }
            }

            $desc = $toggdata['description'];
            $amount = $toggdata['billable'];
            //if(count($name)>0){
                $firstname=$name[0];
                $lastname = $name[1];

                if(!empty($cname) && (!empty($firstname)) && (!empty($lastname))){  // company + name
                    $condition=Capsule::table('tblclients')->where('firstname',$firstname)->where('lastname',$lastname)->where('companyname',$cname)->first();
                    if(!$condition){
                        $condition=Capsule::table('tblclients')->where('firstname',$firstname)->where('lastname',$lastname)->first();
                    }
                    if(!$condition){
                         $condition=Capsule::table('tblclients')->where('companyname',$cname)->first();
                    }
                }else if(!empty($firstname) && !empty($lastname)) {
                //firstname + lasatname
                    $condition=Capsule::table('tblclients')->where('firstname',$firstname)->where('lastname',$lastname)->first();
                }else if(!empty($cname)){ //compnay name
                    $condition=Capsule::table('tblclients')->where('companyname',$cname)->first();

                }
                //print_r($condition);
                //echo Capsule::table('tblclients')->where('firstname',$firstname)->where('lastname',$lastname)->count();
                if($condition){
                    $command = 'AddBillableItem';
                    $admin = Capsule::table('tbladmins')->first();
                    $adminUsername = $admin->id;
                    $postData = array(
                        'clientid' => $condition->id,
                        'description' => date('Y-m-d',strtotime($toggdata['start']))." - ".$toggdata['project']." - ".$toggdata['description']." - ".convertToHoursMins($toggdata['dur'])." x € ".$hourly."/uur",
                        'amount' => $toggdata['billable'],
                        'invoiceaction' => 'nextinvoice',
                        'hours'=>convertToHoursMins($toggdata['dur'])
                    );
                    $results = localAPI($command, $postData, $adminUsername);
                    print_r($results);

                }else{
                     Capsule::table('toggl_details')->insert(
                         [
                         'toggl_id' => $toggdata['id'],
                         'workspace_id'=>$respdata,
                         'name' => $toggdata['user'],
                         'desc' => $desc,
                         'billable' => $amount,
                         'pid' => $toggdata['pid'],
                         'project' => $toggdata['project'],
                         'client' => $toggdata['client'],
                         'start' => $toggdata['start'],
                         'dur' => $toggdata['dur'],
                         'uid' => $toggdata['uid'],
                         'api' => $api,
                         ]
                     );
                }
             // }

         //}
    }
}
echo "Done";
if(Capsule::table('toggl_settings')->where('key','hourly_cron')->first()){
    Capsule::table('toggl_settings')->where('key','hourly_cron')->update([
        'value'=>date('Y-m-d H:i:s')
    ]);
}else{
    Capsule::table('toggl_settings')->insert([
        'key'=>'hourly_cron',
        'value'=>date('Y-m-d H:i:s')
    ]);
}
function convertToHoursMins($init) {
    $hours = floor($init / 1000/3600);
    $minutes = floor(($init / 60) % 60);
    $seconds = $init % 60;

    return "$hours.$minutes";
}
function getTogglSetting($key){
    $result = Capsule::table('toggl_settings')->where('key',$key)->first();
    return $result->value;
}
?>
