<?php
use Illuminate\Database\Capsule\Manager as Capsule;
$data = Capsule::table('toggl_details')->get();
$users = Capsule::table('tblclients')->get();
?>
<div style="text-align: center;padding: 36px;font-size: 26px;"><h1><?php echo $LANG['unmachedrecords']; ?></h1></div>
<table class="table table-striped table-bordered table-hover dt-responsive nowrap" id="example">
<thead>
<tr>
	<th><?php echo $LANG['id']; ?></th>
	<th><?php echo $LANG['username']; ?></th>
	<th><?php echo $LANG['projectid']; ?></th>
	<th><?php echo $LANG['project']; ?></th>
	<th><?php echo $LANG['duration']; ?></th>
	<th><?php echo $LANG['client']; ?></th>
	<th><?php echo $LANG['amount']; ?></th>
	<th><?php echo $LANG['action']; ?></th>
</tr>
</thead>
<tbody>

<?php
foreach($data as $d){
	echo '<tr>';
	echo '<td>'.$d->id.'</td>';
	echo '<td>'.$d->name.'</td>';
	echo '<td>'.$d->pid.'</td>';
	echo '<td>'.$d->project.'</td>';
	echo '<td>'.convertToHoursMins($d->dur).'</td>';
	echo '<td>'.$d->client.'</td>';
	echo '<td>'.$d->billable.'</td>';
	echo '<td><button class="btn btn-success" data-toggle="modal" data-target="#sync_user">'.$LANG['sync'].'</button></td>';
	echo '</tr>';
	?>
	<!-- Modal -->
	<div id="sync_user" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form name="sync_user_data" method="post" id="sync_user_data<?php echo $d->id; ?>" action="addonmodules.php?module=toggl&action=sync_data">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><?php echo $LANG['syncdata']; ?></h4>
					</div>
					<div class="modal-body">
						<div id="results"></div>
						<div id="submitting" style="display: none;"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i> <?php echo $LANG['processwait']; ?></div>

						<div class="form-group">
							<label for="billing"><?php echo $LANG['selectuser']; ?></label>
							<select name="cid" class="form-control">
								<?php foreach($users as $user){
									echo '<option value="'.$user->id.'"> '.$user->firstname.' '.$user->lastname.' ('.$user->email.')'.'</option>';
								} ?>
							</select>
							<input type="hidden" name="id" value="<?php echo $d->id; ?>">
							<input type="hidden" name="desc" value="<?php echo $d->desc; ?>">
							<input type="hidden" name="dur" value="<?php echo $d->dur; ?>">
							<input type="hidden" name="amount" value="<?php echo $d->billable; ?>">
							<input type="hidden" name="start" value="<?php echo $d->start; ?>">
							<input type="hidden" name="workspace_id" value="<?php echo $d->workspace_id; ?>">
							<input type="hidden" name="project" value="<?php echo $d->project; ?>">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" onClick="syncData('sync_user_data<?php echo $d->id; ?>');">Sync</button>
						<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $LANG['close']; ?></button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php
}
?>
<tbody>
</table>
