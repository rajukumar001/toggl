<?php
	$inc = '../modules/addons/toggl/includes/';
	include_once($inc.'action.php');
	$cssPath = '../modules/addons/toggl/css/';
    $jsPath = '../modules/addons/toggl/js/';
	//Custom Script Files
	echo '<script type = "text/javascript" language = "javascript" src = "' . $jsPath . 'bootstrap_dataTable.js"></script>';
	echo '<script type = "text/javascript" language = "javascript" src = "' . $jsPath . 'custom.js"></script>';
	//Custom Style Files
	echo  '<link rel="stylesheet" type="text/css" href="'.$cssPath.'custom.css">';
    echo  '<link rel="stylesheet" type="text/css" href="'.$cssPath.'bootstrap_dataTable.css">';

	if(isset($_GET['action']) && $_GET['action'] == 'view'){
		include_once 'views/view.php';
	}elseif(isset($_GET['action']) && $_GET['action'] == 'setting'){
		include_once 'views/setting.php';
	}else{
		include_once 'views/home.php';
	}

?>
