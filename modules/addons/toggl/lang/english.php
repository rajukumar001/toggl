<?php
$_ADDONLANG['lastcron']="Last Cron Execute At";
$_ADDONLANG['whenexecute']="it will work when workspace does not have hourly rate.";
$_ADDONLANG['submitbutton']="Submit";
$_ADDONLANG['hourlyrate']="Hourly Rate";
$_ADDONLANG['workspaceid']="Workstation ID";

$_ADDONLANG['id']="ID";
$_ADDONLANG['username']="Username";
$_ADDONLANG['projectid']="Project ID";
$_ADDONLANG['workspaceid']="Workstation ID";
$_ADDONLANG['project']="Project";
$_ADDONLANG['duration']="Duration";
$_ADDONLANG['client']="Client";
$_ADDONLANG['amount']="Amount";
$_ADDONLANG['action']="Action";

$_ADDONLANG['syncdata']='Sync Data';
$_ADDONLANG['processwait']='Processing.... Please Wait';
$_ADDONLANG['sync']='Sync';
$_ADDONLANG['close']='Close';
$_ADDONLANG['selectuser']='Select Users';

$_ADDONLANG['home']='Home';
$_ADDONLANG['unuseddata']='Unsync Data';
$_ADDONLANG['settings']='Settings';
$_ADDONLANG['label']='Label';

$_ADDONLANG['unmachedrecords']='Unmatched time records';

?>
