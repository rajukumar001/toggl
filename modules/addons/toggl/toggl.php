<?php
use Illuminate\Database\Capsule\Manager as Capsule;

if (!defined("WHMCS"))
    die("This file cannot be accessed directly");


function toggl_config() {
    $configarray = array(
        "name" => "Toggl",
        "description" => "This addon is for adding updating and deleting",
        "version" => "1.0",
        "author" => "WHMCS7",
        "language" => "English",
        'fields' => array(
            // a text field type allows for single line text input
            'Api_token' => array(
                'FriendlyName' => 'API Token',
                'Type' => 'text',
                'Size' => '40',
                'Default' => '',
                'Description' => 'toggl API Token',
            ),
        )
    );
    return $configarray;
}

// Create a new table.
function toggl_activate() {
   try {
    Capsule::schema()->create(
        'toggl_details',
        function ($table) {
            /** @var \Illuminate\Database\Schema\Blueprint $table */
            $table->increments('id');
            $table->integer('toggl_id');
            $table->integer('whmcs_uid');
            $table->string('name');
            $table->string('desc');
            $table->string('billable')->nullable();
            $table->integer('pid');
            $table->string('project')->nullable();
            $table->string('client')->nullable();
            $table->string('start')->nullable();
            $table->integer('dur');
            $table->integer('uid');
            $table->integer('api');
        }
        );
    Capsule::schema()->create(
        'toggl_settings',
        function ($table) {
            /** @var \Illuminate\Database\Schema\Blueprint $table */
            $table->increments('id');
            $table->string('key');
            $table->string('value');
            $table->integer('status');
        }
        );

} catch (\Exception $e) {
    return array(
        'status' => 'eror',
        'description' => "Unable to create toggl_details: {$e->getMessage()}",
        );
}
return array(
    'status' => 'success',
    'description' => 'Module Activated SuccessFully',
    );
}
//Deactivate Table
function toggl_deactivate() {
    Capsule::schema()->dropIfExists('toggl_details');
    Capsule::schema()->dropIfExists('toggl_settings');
    return array(
        'status' => 'success',
        'description' => 'Module Deactivated SuccessFully',
        );
}
// Main Function for output
function toggl_output($vars) {
    $LANG = $vars['_lang'];
	$inc = '../modules/addons/toggl/includes/';
    require_once $inc.'/function.php';
	require_once($inc.'action.php');
	$cssPath = '../modules/addons/toggl/css/';
    $jsPath = '../modules/addons/toggl/js/';
	//Custom Script Files


    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $cssPath; ?>custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $cssPath; ?> bootstrap_dataTable.css">
    <ul class="list-inline text-center">
     <li><a class="btn <?php echo !isset($_GET['action']) ? 'btn-primary' : 'btn-default'; ?>" href="addonmodules.php?module=toggl"><?php echo $LANG['home']; ?></a></li>
     <li><a class="btn <?php echo $_GET['action'] == 'view' ? 'btn-primary' : 'btn-default'; ?>" href="addonmodules.php?module=toggl&action=view"><?php echo $LANG['unuseddata']; ?></a></li>
     <li><a class="btn <?php echo $_GET['action'] == 'setting' ? 'btn-primary' : 'btn-default';?>" href="addonmodules.php?module=toggl&action=setting"><?php echo $LANG['settings']; ?></a></li>
 </ul>
 <?php

if(isset($_GET['action']) &&  $_GET['action'] == 'view'){
  include_once 'views/view.php';
}elseif(isset($_GET['action']) &&  $_GET['action'] == 'setting'){
  include_once 'views/setting.php';
}else{
  include_once 'views/home.php';
}
echo '<script type = "text/javascript" language = "javascript" src = "' . $jsPath . 'bootstrap_dataTable.js"></script>';
echo '<script type = "text/javascript" language = "javascript" src = "' . $jsPath . 'custom.js"></script>';
}
?>
