jQuery(document).ready(function() {
    jQuery('#example').dataTable( {
		"columnDefs": [ {
		  "targets"  : 'no-sort',
		  "orderable": false,
		  "order": []
		}]
	});
});



function syncData(form_id){
  //alert( "Handler for .submit() called." );
  event.preventDefault();
  //var form_id = 'add_did';
  //alert(form_id);
  formSubmitter(form_id);
}

function formSubmitter(form_id){
    var url = $("#"+form_id).attr("action");
    //alert(url);
    //alert(form_id);
    var data = $("#"+form_id).serialize();
    //alert(data);

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        beforeSend: function() {
            jQuery('#submitting').show();
        },
        success: function(result) {
            if(result=='success'){
                jQuery('#submitting').hide();
                var results = $("#"+form_id).find("#results");

                if(result == 'success'){
                    var res = '<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Invoice has synced.</div>';
                }else{
                    var res = '<div class="alert alert-success alert-dismissable"><strong>Error! </strong> Something Went Wrong. Try with new values.</div>';
                }
                $("#"+form_id).find("#results").html(res);
                window.location.reload();
            }
        }
    });
}
