<?php
use Illuminate\Database\Capsule\Manager as Capsule;

if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'sync_data'){
    $postDatas = array(
        "workspace_id" => $_REQUEST['workspace_id']
    );
    $defaulthourly=getTogglSetting('hourly_rate');
    $hourlyrate=run_curl_workstation($postDatas);
    $hourly=$hourlyrate['data']['default_hourly_rate']==0?$defaulthourly:$hourlyrate['data']['default_hourly_rate'];
	$command = 'AddBillableItem';
	$admin = Capsule::table('tbladmins')->first();
	$adminUsername = $admin->id;
	$postData = array(
        'clientid' => $_REQUEST['cid'],
        'description' => date('Y-m-d',strtotime($_REQUEST['start']))." - ".$_REQUEST['project']." - ".$_REQUEST['desc']." - ".convertToHoursMins($_REQUEST['dur'])." x € ".$hourly."/uur",
        'amount' => $_REQUEST['amount'],
        'invoiceaction' => 'nextinvoice',
        'hours'=>convertToHoursMins($_REQUEST['dur'])
    );
   // print_r($postData); exit();
	$results = localAPI($command, $postData, $adminUsername);
	//print_r($results); die;
	if($results['result'] == 'success'){
		Capsule::table('toggl_details')->where('id', $_REQUEST['id'])->delete();
		echo 'success'; die;
	}else{
		echo "error"; die;
	}
	exit();
}
?>
